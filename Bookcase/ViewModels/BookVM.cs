﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BookcaseData.EntityModels;

namespace Bookcase.ViewModels
{
    public class BookVM
    {
        [Required]
        [Display(Name = "Tytuł")]
        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Autor")]
        [StringLength(50, MinimumLength = 5)]
        public string Writer { get; set; }
        [Required]
        [Display(Name = "Gatunek")]
        public string Genre { get; set; }
        [Display(Name = "Właściciel")]
        public int OwnerId { get; set; }
        public int Id { get; set; }

        public virtual ICollection<Rate> Rates { get; set; }
    }
}
