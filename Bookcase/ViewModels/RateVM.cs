﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bookcase.ViewModels
{
    public class RateVM
    {
        [Display(Name = "Ocena")]
        [Range(0, 10)]
        [DisplayFormat(NullDisplayText = "Brak oceny")]
        public int Rating { get; set; }
        [Display(Name = "Książka")]
        public int BookId { get; set; }
        [Display(Name = "Wypożyczający")]
        public int PersonId { get; set; }
        public int Id { get; set; }

        [Display(Name = "Tytuł książki")]
        public string BookName { get; set; }
        [Display(Name = "Wypożyczający")]
        public string PersonName { get; set; }


        public List<SelectListItem> UsersSelectList { get; set; }
        public List<SelectListItem> BooksSelectList { get; set; }
    }
}