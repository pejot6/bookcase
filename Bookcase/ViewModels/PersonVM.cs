﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bookcase.ViewModels
{
    public class PersonVM
    {
        [Required]
        [StringLength(15, ErrorMessage = "First name cannot be longer than 15 characters.")]
        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Last name cannot be longer than 50 characters.")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }
        public int Id { get; set; }
    }
}