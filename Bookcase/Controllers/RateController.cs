﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bookcase.ViewModels;
using BookcaseData.DAL;
using BookcaseData.EntityModels;
using BookcaseData.Repositories;
using PagedList;

namespace Bookcase.Controllers
{
    public class RateController : Controller
    {
        private readonly IRateRepository _rateRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IBookRepository _bookRepository;

        public RateController()
        {
            this._rateRepository = new RateRepository(new BookcaseContext());
            this._personRepository = new PersonRepository(new BookcaseContext());
            this._bookRepository = new BookRepository(new BookcaseContext());
        }

        // GET: Rate
        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "nameDescending" : "";
            ViewBag.WriterSortParm = sortOrder == "Writer" ? "writerDescending" : "Writer";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var allrates = _rateRepository.GetAll();

            if (!string.IsNullOrEmpty(searchString))
            {
                allrates = allrates.Where(s => s.BookId.ToString().ToLower().Contains(searchString) || s.PersonId.ToString().ToLower().Contains(searchString));

            }

            switch (sortOrder)
            {
                case "nameDescending":
                    allrates = allrates.OrderByDescending(b => b.Id);
                    break;
                case "Writer":
                    allrates = allrates.OrderBy(b => b.BookId);
                    break;
                case "writerDescending":
                    allrates = allrates.OrderByDescending(b => b.BookId);
                    break;
                default:
                    allrates = allrates.OrderBy(b => b.Id);
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            List<RateVM> rateVms = new List<RateVM>();
            foreach (var rate in allrates)
            {
                rateVms.Add(MapToVm(rate));
            }

            return View(rateVms.ToPagedList(pageNumber, pageSize));
        }

        private static RateVM MapToVm(Rate rate)
        {
            return new RateVM
            {
                Rating = rate.Rating,
                BookId = rate.BookId,
                PersonId = rate.PersonId,
                Id = rate.Id,
                PersonName = rate.Person.Fullname,
                BookName = rate.Book.Title
            };
        }

        private static void MapFromVm(RateVM vm, Rate entity)
        {
            entity.Rating = vm.Rating;
            entity.BookId = vm.BookId;
            entity.PersonId = vm.PersonId;
            entity.Id = vm.Id;
        }

        //GET
        public ActionResult AddRate()
        {
            var rateVm = new RateVM();
            SetUsersSelectList(rateVm);
            SetBooksSelectList(rateVm);

            return View("AddRate", rateVm);
        }

        private void SetUsersSelectList(RateVM vm)
        {
            vm.UsersSelectList = _personRepository.GetAll().Select(x => new SelectListItem() { Text = x.Fullname, Value = x.Id.ToString() }).ToList();
        }
        private void SetBooksSelectList(RateVM vm)
        {
            vm.BooksSelectList = _bookRepository.GetAll().Select(x => new SelectListItem() { Text = x.Title, Value = x.Id.ToString() }).ToList();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddRate(RateVM rateVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var rate = new Rate();
                    MapFromVm(rateVm, rate);
                    _rateRepository.Add(rate);
                    _rateRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
            }
            return View(rateVm);
        }

        //GET
        public ActionResult UpdateRate(int id)
        {

            var rate = _rateRepository.GetById(id);
            if (rate == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(rate));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateRate(RateVM rateVm)
        {
            if (rateVm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                var rate = _rateRepository.GetById(rateVm.Id);
                MapFromVm(rateVm, rate);
                _rateRepository.Update(rate);

                try
                {
                    _rateRepository.Save();
                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
                }
            }
            return View(rateVm);
        }

        //GET
        public ActionResult DeleteRate(int id, bool? saveChanges = false)
        {
            if (saveChanges.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Błąd podczas usuwania, spróbuj ponownie";
            }
            var rate = _rateRepository.GetById(id);

            if (rate == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(rate));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteRate(int id)
        {
            try
            {
                _rateRepository.Delete(id);
                _rateRepository.Save();

            }
            catch (RetryLimitExceededException)
            {
                return RedirectToAction("DeleteRate", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }
    }
}