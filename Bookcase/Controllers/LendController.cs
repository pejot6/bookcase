﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bookcase.ViewModels;
using BookcaseData.DAL;
using BookcaseData.EntityModels;
using BookcaseData.Repositories;
using PagedList;


namespace Bookcase.Controllers
{
    public class LendController : Controller
    {
        private readonly ILendRepository _lendRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IBookRepository _bookRepository;

        public LendController()
        {
            this._lendRepository = new LendRepository(new BookcaseContext());
            this._personRepository = new PersonRepository(new BookcaseContext());
            this._bookRepository = new BookRepository(new BookcaseContext());
        }


        // GET: Lend
        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "dateDescending" : "";
            ViewBag.NameSortParm = sortOrder == "Title" ? "titleDescending" : "Title";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var alllends = _lendRepository.GetAll();
            if (!string.IsNullOrEmpty(searchString))
            {
                alllends =
                    alllends.Where(
                        s =>
                            s.BookId.ToString().ToLower().Contains(searchString) ||
                            s.PersonId.ToString().ToLower().Contains(searchString));
            }

            switch (sortOrder)
            {
                case "dateDescending":
                    alllends = alllends.OrderByDescending(b => b.Id);
                    break;
                case "Title":
                    alllends = alllends.OrderBy(b => b.BookId);
                    break;
                case "titleDescending":
                    alllends = alllends.OrderByDescending(b => b.BookId);
                    break;
                default:
                    alllends = alllends.OrderBy(b => b.Id);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            List<LendVM> lendVms = new List<LendVM>();
            foreach (var lend in alllends)
            {
                lendVms.Add(MapToVm(lend));
            }


            return View(lendVms.ToPagedList(pageNumber, pageSize));
        }

        private static LendVM MapToVm(Lend lend)
        {
            return new LendVM
            {
                BorrowDateTime = lend.BorrowDateTime,
                ReturnDateTime = lend.ReturnDateTime,
                BookId = lend.BookId,
                PersonId = lend.PersonId,
                Id = lend.Id,
                BookName = lend.Book.Title,
                PersonName = lend.Person.Fullname
            };
        }

        private static void MapFromVm(LendVM vm, Lend entity)
        {
            entity.BorrowDateTime = vm.BorrowDateTime;
            entity.ReturnDateTime = vm.ReturnDateTime;
            entity.BookId = vm.BookId;
            entity.PersonId = vm.PersonId;
            entity.Id = vm.Id;
        }



        //GET
        public ActionResult AddLend()
        {
            var lendVm = new LendVM();
            SetUsersSelectList(lendVm);
            SetBooksSelectList(lendVm);


            return View("AddLend", lendVm);
        }

        private void SetUsersSelectList(LendVM vm)
        {
            vm.UsersSelectList =
                _personRepository.GetAll()
                    .Select(x => new SelectListItem() { Text = x.Fullname, Value = x.Id.ToString() })
                    .ToList();
        }

        private void SetBooksSelectList(LendVM vm)
        {
            vm.BooksSelectList =
                _bookRepository.GetAll()
                    .Select(x => new SelectListItem() { Text = x.Title, Value = x.Id.ToString() })
                    .ToList();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddLend(LendVM lendVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var lend = new Lend();
                    MapFromVm(lendVm, lend);
                    _lendRepository.Add(lend);
                    _lendRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
            }

            return View(lendVm);
        }

        [HttpPost]
        public JsonResult PostPerson(PersonVM personVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var person = new Person();
                    MapFromVm(personVm, person);
                    _personRepository.Add(person);
                    _personRepository.Save();
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
            }


            return Json(personVm);
        }
        private static void MapFromVm(PersonVM vm, Person entity)
        {
            entity.Name = vm.Name;
            entity.LastName = vm.LastName;
            entity.Id = vm.Id;
        }


        //GET
        public ActionResult UpdateLend(int id)
        {

            var lend = _lendRepository.GetById(id);
            if (lend == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(lend));


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateLend(LendVM lendVm)
        {
            if (lendVm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                var lend = _lendRepository.GetById(lendVm.Id);
                MapFromVm(lendVm, lend);
                _lendRepository.Update(lend);

                try
                {
                    _lendRepository.Save();
                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
                }
            }
            return View(lendVm);
        }

        //GET
        public ActionResult DeleteLend(int id, bool? saveChanges = false)
        {
            if (saveChanges.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Błąd podczas usuwania, spróbuj ponownie";
            }
            var lend = _lendRepository.GetById(id);

            if (lend == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(lend));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteLend(int id)
        {
            try
            {
                _lendRepository.Delete(id);
                _lendRepository.Save();

            }
            catch (RetryLimitExceededException)
            {
                return RedirectToAction("DeleteLend", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }





    }
}