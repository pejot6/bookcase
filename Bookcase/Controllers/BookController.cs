﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;
using System.Web.UI.WebControls;
using BookcaseData.DAL;
using BookcaseData.EntityModels;
using BookcaseData.Repositories;
using Bookcase.ViewModels;
using PagedList;

namespace Bookcase.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookRepository _bookRepository;

        public BookController()
        {
            this._bookRepository = new BookRepository(new BookcaseContext());
        }

        

        // GET: Book
        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {

            //ViewBag.BookID = id;
           
           
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "nameDescending" : "";
            ViewBag.WriterSortParm = sortOrder == "Writer" ? "writerDescending" : "Writer";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var allbooks = _bookRepository.GetAll();

            if (!string.IsNullOrEmpty(searchString))
            {
                allbooks = allbooks.Where(s => s.Title.ToLower().Contains(searchString) || s.Writer.ToLower().Contains(searchString));
                
            }

            switch (sortOrder)
            {
                case "nameDescending":
                   allbooks = allbooks.OrderByDescending(b => b.Title);
                    break;
                case "Writer":
                    allbooks = allbooks.OrderBy(b => b.Writer);
                    break;
                case "writerDescending":
                    allbooks = allbooks.OrderByDescending(b => b.Writer);
                    break;
                default:
                    allbooks = allbooks.OrderBy(b => b.Title);
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            List<BookVM> booksVms = new List<BookVM>();
            foreach (var book in allbooks)
            {
                booksVms.Add(MapToVm(book));
            }

            return View(booksVms.ToPagedList(pageNumber, pageSize));
        }

        //GET
        public ActionResult AddBook()
        {
            return View("AddBook");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBook(BookVM bookVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var book = new Book();
                    MapFromVm(bookVm, book);
                    _bookRepository.Add(book);
                    _bookRepository.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
            }
            return View(bookVm);
        }

        //GET
        public ActionResult UpdateBook(int id)
        {

            var book = _bookRepository.GetById(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(book));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateBook(BookVM bookVm)
        {
            if (bookVm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                var book = _bookRepository.GetById(bookVm.Id);
                MapFromVm(bookVm, book);
                _bookRepository.Update(book);

                try
                {
                    _bookRepository.Save();
                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
                }
            }
            return View(bookVm);
        }

        //GET
        public ActionResult DeleteBook(int id, bool? saveChanges = false)
        {
            if (saveChanges.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Błąd podczas usuwania, spróbuj ponownie";
            }
            var book = _bookRepository.GetById(id);
            
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(book));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteBook(int id)
        {
            try
            {
                _bookRepository.Delete(id);
                _bookRepository.Save();
                
            }
            catch (RetryLimitExceededException)
            {
                return RedirectToAction("DeleteBook", new {id=id, saveChangesError=true});
            }
            return RedirectToAction("Index");
        }




        private static BookVM MapToVm(Book book)
        {
            return new BookVM
            {
                Title = book.Title,
                Writer = book.Writer,
                Genre = book.Genre,
                OwnerId = book.OwnerId,
                Id = book.Id
                
            };
        }

        private static void MapFromVm(BookVM vm, Book entity)
        {
            entity.Title = vm.Title;
            entity.Writer = vm.Writer;
            entity.Genre = vm.Genre;
            entity.OwnerId = vm.OwnerId;
            entity.Id = vm.Id;
        }

      
    }
}