﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bookcase.ViewModels;
using BookcaseData.DAL;
using BookcaseData.EntityModels;
using BookcaseData.Repositories;
using PagedList;

namespace Bookcase.Controllers
{
    public class PersonController : Controller
    {
        private readonly IPersonRepository _personRepository;

        public PersonController()
        {
            this._personRepository = new PersonRepository(new BookcaseContext());
        }


        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "nameDescending" : "";
            ViewBag.WriterSortParm = sortOrder == "LastName" ? "lastNameDescending" : "LastName";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var allpersons = _personRepository.GetAll();

            if (!string.IsNullOrEmpty(searchString))
            {
                allpersons = allpersons.Where(s => s.Name.ToLower().Contains(searchString) || s.LastName.ToLower().Contains(searchString));

            }

            switch (sortOrder)
            {
                case "nameDescending":
                    allpersons = allpersons.OrderByDescending(b => b.Name);
                    break;
                case "LastName":
                    allpersons = allpersons.OrderBy(b => b.LastName);
                    break;
                case "lastNameDescending":
                    allpersons = allpersons.OrderByDescending(b => b.LastName);
                    break;
                default:
                    allpersons = allpersons.OrderBy(b => b.Name);
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            List<PersonVM> peoplesVms = new List<PersonVM>();
            foreach (var person in allpersons)
            {
                peoplesVms.Add(MapToVm(person));
            }

            return View(peoplesVms.ToPagedList(pageNumber, pageSize));
        }

        //GET
        [HttpGet]
        public ActionResult AddPerson(string returnAction, string returnController)
        {
            ViewBag.ReturnUrl = returnAction;
            ViewBag.ReturnController = returnController;
            return View("AddPerson");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPerson(PersonVM personVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var person = new Person();
                    MapFromVm(personVm, person);
                    _personRepository.Add(person);
                    _personRepository.Save();
                    if (!string.IsNullOrEmpty(ViewBag.ReturnUrl))
                    {
                        return RedirectToAction(ViewBag.ReturnUrl, ViewBag.ReturnController);
                    }

                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
            }
            return View(personVm);
        }

        //GET
        public ActionResult UpdatePerson(int id)
        {

            var person = _personRepository.GetById(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(person));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePerson(PersonVM personVm)
        {
            if (personVm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                var book = _personRepository.GetById(personVm.Id);
                MapFromVm(personVm, book);
                _personRepository.Update(book);

                try
                {
                    _personRepository.Save();
                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Wystąpił błąd zapisu, spróbuj ponownie");
                }
            }
            return View(personVm);
        }

        //GET
        public ActionResult DeletePerson(int id, bool? saveChanges = false)
        {
            if (saveChanges.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Błąd podczas usuwania, spróbuj ponownie";
            }
            var person = _personRepository.GetById(id);

            if (person == null)
            {
                return HttpNotFound();
            }
            return View(MapToVm(person));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePerson(int id)
        {
            try
            {
                _personRepository.Delete(id);
                _personRepository.Save();

            }
            catch (RetryLimitExceededException)
            {
                return RedirectToAction("DeletePerson", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetPersons()
        {
            var allPersons = _personRepository.GetAll();

            var list =
                new List<dynamic>() { new { Value = -1, Text = "--Wybierz--" } }.Union(
                    allPersons.Select(x => new { Value = x.Id, Text = x.Fullname }));

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        private static PersonVM MapToVm(Person person)
        {
            return new PersonVM()
            {
                Name = person.Name,
                LastName = person.LastName,
                Id = person.Id

            };
        }

        private static void MapFromVm(PersonVM vm, Person entity)
        {
            entity.Name = vm.Name;
            entity.LastName = vm.LastName;
            entity.Id = vm.Id;
        }
    }
}