﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace Bookcase.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            using (BookcaseContext db = new BookcaseContext())
            {
                return View(db.UserAccounts.ToList());
            }
            //return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(UserAccount account)
        {
            if (ModelState.IsValid)
            {
                using (BookcaseContext db = new BookcaseContext())
                {
                    db.UserAccounts.Add((account));
                    db.SaveChanges();
                }
                ModelState.Clear();
                ViewBag.Message = "Rejestracje użtkownika " + account.UserName + " zakończona sukcesem!";
            }
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserAccount user)
        {
            using (BookcaseContext db = new BookcaseContext())
            {
                //var usr = db.UserAccounts.Single(u => u.UserName == user.UserName && u.Password == user.Password);
                var usr =
                    db.UserAccounts
                        .FirstOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);
                if (usr != null)
                {
                    Session["UserId"] = usr.UserId.ToString();
                    Session["UserName"] = usr.UserName;
                    FormsAuthentication.SetAuthCookie(usr.UserName, true);

                    return RedirectToAction("LoggedIn");
                }
                else
                {
                    ModelState.AddModelError("", "Nazwa użytkownika albo hasło błędne.");
                }
            }
            return View();
        }

        public ActionResult LoggedIn()
        {
            if (Session["UserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}