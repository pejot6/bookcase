﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookcaseData.DAL
{
    public abstract class Entity : IEntity
    {
        public int Id { get; set; }
    }
}