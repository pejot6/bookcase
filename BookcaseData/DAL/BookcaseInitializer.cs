﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookcaseData.EntityModels;
using System.Data.Entity;
using System.Data.Entity.Migrations;


namespace BookcaseData.DAL
{
    public class BookcaseInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BookcaseContext>
    {

        private void fillBookList(BookcaseContext context)
        {
            List<Book> books = new List<Book>();
            int id = 1;
            books.Add(new Book()
            {
                Id = id++,
                Title = "Sługa Boży",
                Writer = "Jacek Piekara",
                Genre = "Fantasy"
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Elon Musk",
                Writer = "Ashlee Vance",
                Genre = "Bibliography"
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Powrót z Gwiazd",
                Writer = "Stanisław Lem",
                Genre = "Sci-Fi"
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Martin Eden",
                Writer = "Jack London",
                Genre = "Novel"
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Ostatnie Życzenie",
                Writer = "Andrzej Sapkowski",
                Genre = "Fantasy"
            });
            foreach (var item in books)
            {
                context.Books.Add(item);
            }
            context.SaveChanges();
        }

        private void fillPersonList(BookcaseContext context)
        {
          List<Person> persons = new List<Person>();
            int id = 1;
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Paweł",
                LastName = "Jędrysiak"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Żaneta",
                LastName = "Krawczyk"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Piotr",
                LastName = "Wiśniewski"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Paweł",
                LastName = "Jaworski"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Łukasz",
                LastName = "Kłos"
            });
            foreach (var item in persons)
            {
                context.Persons.Add(item);
            }
            context.SaveChanges();
        }

        private void fillLendList(BookcaseContext context)
        {
            List<Lend> lends = new List<Lend>();
            int id = 1;
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17")
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17")
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17")
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-01"),
                ReturnDateTime = DateTime.Parse("2017-01-07")
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17")
            });
            foreach (var item in lends)
            {
                context.Lends.Add(item);
            }
            context.SaveChanges();
        }

        private void fillRateList(BookcaseContext context)
        {
            List<Rate> rates = new List<Rate>();
            int id = 1;
            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 10
            });
            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 9
            });
            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 9
            });
            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 8
            });
            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 9
            });
            foreach (var item in rates)
            {
                context.Rates.Add(item);
            }
            context.SaveChanges();
        }
        

        //protected override void Seed(BookcaseContext context)
        //{
        //    fillBookList(context);
        //    fillLendList(context);
        //    fillPersonList(context);
        //    fillRateList(context);

        //    base.Seed(context);


            //var persons = new List<Person>
            //{
            //    new Person {Name = "Paweł", LastName = "Jędrysiak", Id = 1},
            //    new Person {Name = "Żaneta", LastName = "Krawczyk", Id = 2},
            //    new Person {Name = "Piotr", LastName = "Wiśniewski", Id = 3},
            //    new Person {Name = "Paweł", LastName = "Jaworski", Id=4},
            //    new Person {Name = "Łukasz", LastName = "Kłos", Id=5},
            //};

            //persons.ForEach(b => context.Persons.AddOrUpdate(b));
            //context.SaveChanges();


            //var books = new List<Book>
            //{
            //    new Book {Title = "Sługa Boży", Writer = "Jacek Piekara", Genre = "Fantasy", PersonId = persons.Single(p=>p.LastName == "Jędrysiak").Id},
            //    new Book {Title = "Elon Musk", Writer = "Ashlee Vance", Genre = "Bibliography", PersonId = persons.Single(p=>p.LastName == "Krawczyk").Id},
            //    new Book {Title = "Powrót z Gwiazd", Writer = "Stanisław Lem", Genre = "Sci-Fi", PersonId = persons.Single(p=>p.LastName == "Wiśniewski").Id},
            //    new Book {Title = "Martin Eden", Writer = "Jack London", Genre = "Novel", PersonId = persons.Single(p=>p.LastName == "Jaworski").Id},
            //    new Book {Title = "Ostatnie Życzenie", Writer = "Andrzej Sapkowski", Genre = "Fantasy", PersonId = persons.Single(p=>p.LastName == "Kłos").Id},

            //};
            //books.ForEach(s => context.Books.AddOrUpdate(s));
            //context.SaveChanges();

            //var lends = new List<Lend>
            //{
            //    new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Sługa Boży").Id},
            //    new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Elon Musk").Id},
            //    new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Powrót z Gwiazd").Id},
            //    new Lend {BorrowDateTime = DateTime.Parse("2017-01-01"), ReturnDateTime = DateTime.Parse("2017-01-07"), BookId = books.Single(b=>b.Title == "Martin Eden").Id},
            //    new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Ostatnie Życzenie").Id},

            //};
            //lends.ForEach(s => context.Lends.AddOrUpdate(s));
            //context.SaveChanges();

            //var rates = new List<Rate>
            //{
            //    new Rate {Rating = 10, BookId = books.Single(b=>b.Title == "Sługa Boży").Id},
            //    new Rate {Rating = 9, BookId = books.Single(b=>b.Title == "Elon Musk").Id},
            //    new Rate {Rating = 9, BookId = books.Single(b=>b.Title == "Powrót z Gwiazd").Id},
            //    new Rate {Rating = 8, BookId = books.Single(b=>b.Title == "Martin Eden").Id},
            //    new Rate {Rating = 9, BookId = books.Single(b=>b.Title == "Ostatnie Życzenie").Id},
            //};
            //rates.ForEach(s => context.Rates.AddOrUpdate(s));
            //context.SaveChanges();

             protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Book>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Book>()
                .HasRequired<Person>(s => s.Owner)
                .WithMany(s => s.Books)
                .HasForeignKey(s => s.OwnerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>()
                .HasMany(x => x.Rates)
                .WithRequired(x => x.Book)
                .HasForeignKey(x => x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>().Property(x => x.Title)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Book>().Property(x => x.Writer)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Book>().Property(x => x.Genre)
                .IsRequired();

            modelBuilder.Entity<Person>()
                .HasMany(x => x.Books)
                .WithRequired(x => x.Owner)
                .HasForeignKey(x => x.OwnerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .HasMany(x => x.Rates)
                .WithRequired(x => x.Person)
                .HasForeignKey(x => x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .HasMany(x => x.Lends)
                .WithRequired(x => x.Person)
                .HasForeignKey(x => x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .Property(x => x.Name)
                .HasMaxLength(15);

            modelBuilder.Entity<Person>()
                .Property(x => x.LastName)
                .HasMaxLength(30);

            modelBuilder.Entity<Person>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Lend>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Lend>()
                .Property(x => x.BorrowDateTime);

            modelBuilder.Entity<Lend>()
                .Property(x => x.ReturnDateTime);

            modelBuilder.Entity<Lend>()
                .HasRequired(x => x.Book)
                .WithMany(x => x.Lends)
                .HasForeignKey(x => x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lend>()
                .HasRequired(x => x.Person)
                .WithMany(x => x.Lends)
                .HasForeignKey(x => x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rate>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Rate>()
                .Property(x => x.Rating);

            modelBuilder.Entity<Rate>()
                .HasRequired(x => x.Book)
                .WithMany(x => x.Rates)
                .HasForeignKey(x => x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rate>()
                .HasRequired(x => x.Person)
                .WithMany(x => x.Rates)
                .HasForeignKey(x => x.PersonId)
                .WillCascadeOnDelete(false);
        }
    }
    }
