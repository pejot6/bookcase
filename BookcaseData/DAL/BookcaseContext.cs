﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using BookcaseData.EntityModels;

namespace BookcaseData.DAL
{
    public class BookcaseContext : DbContext
    {
        public BookcaseContext() : base("BookcaseConnectionString")
        { }

        public DbSet<Book> Books { get; set; }
        public DbSet<Lend> Lends { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Book
            modelBuilder.Entity<Book>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Book>()
                .HasRequired(x=>x.Owner)
                .WithMany(x=>x.Books)
                .HasForeignKey(x=>x.OwnerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>()
                .HasMany(x=>x.Rates)
                .WithRequired(x=>x.Book)
                .HasForeignKey(x=>x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>()
                .HasMany(x=>x.Lends)
                .WithRequired(x=>x.Book)
                .HasForeignKey(x=>x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Book>().Property(x => x.Title)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Book>().Property(x => x.Writer)
               .HasMaxLength(50)
               .IsRequired();

            modelBuilder.Entity<Book>().Property(x => x.Genre)
                .IsRequired();

            modelBuilder.Entity<Book>().Property(x => x.OwnerId)
               .IsRequired();

            //Person
            modelBuilder.Entity<Person>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Person>()
                .HasMany(x=>x.Books)
                .WithRequired(x=>x.Owner)
                .HasForeignKey(x=>x.OwnerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .HasMany(x=>x.Lends)
                .WithRequired(x=>x.Person)
                .HasForeignKey(x=>x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .HasMany(x=>x.Rates)
                .WithRequired(x=>x.Person)
                .HasForeignKey(x=>x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>().Property(x => x.Name)
                .HasMaxLength(15)
                .IsRequired();

            modelBuilder.Entity<Person>().Property(x => x.LastName)
                .HasMaxLength(30)
                .IsRequired();
            
            //Lend
            modelBuilder.Entity<Lend>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Lend>()
                .HasRequired<Book>(x=>x.Book)
                .WithMany(x=>x.Lends)
                .HasForeignKey(x=>x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lend>()
                .HasRequired<Person>(x=>x.Person)
                .WithMany(x=>x.Lends)
                .HasForeignKey(x=>x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lend>().Property(x => x.BorrowDateTime)
                .IsRequired();

            modelBuilder.Entity<Lend>().Property(x => x.ReturnDateTime)
                .IsRequired();
            
            //Rate
            modelBuilder.Entity<Rate>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Rate>()
                .HasRequired<Book>(x=>x.Book)
                .WithMany(x=>x.Rates)
                .HasForeignKey(x=>x.BookId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rate>()
                .HasRequired<Person>(x=>x.Person)
                .WithMany(x=>x.Rates)
                .HasForeignKey(x=>x.PersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rate>().Property(x => x.Rating);

        }
    }
}