namespace BookcaseData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Writer = c.String(nullable: false, maxLength: 50),
                        Genre = c.String(nullable: false),
                        OwnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.OwnerId)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.Lend",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BorrowDateTime = c.DateTime(nullable: false),
                        ReturnDateTime = c.DateTime(nullable: false),
                        IsLoaned = c.Boolean(nullable: false),
                        BookId = c.Int(nullable: false),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .ForeignKey("dbo.Book", t => t.BookId)
                .Index(t => t.BookId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 15),
                        LastName = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rating = c.Int(nullable: false),
                        BookId = c.Int(nullable: false),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .ForeignKey("dbo.Book", t => t.BookId)
                .Index(t => t.BookId)
                .Index(t => t.PersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rate", "BookId", "dbo.Book");
            DropForeignKey("dbo.Book", "OwnerId", "dbo.Person");
            DropForeignKey("dbo.Lend", "BookId", "dbo.Book");
            DropForeignKey("dbo.Lend", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Rate", "PersonId", "dbo.Person");
            DropIndex("dbo.Rate", new[] { "PersonId" });
            DropIndex("dbo.Rate", new[] { "BookId" });
            DropIndex("dbo.Lend", new[] { "PersonId" });
            DropIndex("dbo.Lend", new[] { "BookId" });
            DropIndex("dbo.Book", new[] { "OwnerId" });
            DropTable("dbo.Rate");
            DropTable("dbo.Person");
            DropTable("dbo.Lend");
            DropTable("dbo.Book");
        }
    }
}
