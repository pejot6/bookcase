﻿using System.Collections.Generic;
using System.Diagnostics;
using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace BookcaseData.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BookcaseData.DAL.BookcaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private void FillBookList(BookcaseContext context)
        {
            List<Book> books = new List<Book>();
            int id = 1;
            books.Add(new Book()
            {
                Id = id++,
                Title = "Sługa Boży",
                Writer = "Jacek Piekara",
                Genre = "Fantasy",
                OwnerId = 1
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Elon Musk",
                Writer = "Ashlee Vance",
                Genre = "Bibliography",
                OwnerId = 1
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Powrót z Gwiazd",
                Writer = "Stanisław Lem",
                Genre = "Sci-Fi",
                OwnerId = 1
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Martin Eden",
                Writer = "Jack London",
                Genre = "Novel",
                OwnerId = 1
            });
            books.Add(new Book()
            {
                Id = id++,
                Title = "Ostatnie Życzenie",
                Writer = "Andrzej Sapkowski",
                Genre = "Fantasy",
                OwnerId = 1
            });
            foreach (var item in books)
            {
                context.Books.AddOrUpdate(item);
            }

        }

        private void FillPersonList(BookcaseContext context)
        {
            List<Person> persons = new List<Person>();
            int id = 1;
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Paweł",
                LastName = "Jędrysiak"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Żaneta",
                LastName = "Krawczyk"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Piotr",
                LastName = "Wiśniewski"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Paweł",
                LastName = "Jaworski"
            });
            persons.Add(new Person()
            {
                Id = id++,
                Name = "Łukasz",
                LastName = "Kłos"
            });
            foreach (var item in persons)
            {
                context.Persons.AddOrUpdate(item);
            }

        }

        private void FillLendList(BookcaseContext context)
        {
            List<Lend> lends = new List<Lend>();
            int id = 1;
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17"),
                PersonId = 2,
                BookId = 1
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17"),
                PersonId = 2,
                BookId = 1
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17"),
                PersonId = 2,
                BookId = 1
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-01"),
                ReturnDateTime = DateTime.Parse("2017-01-07"),
                PersonId = 2,
                BookId = 1
            });
            lends.Add(new Lend()
            {
                Id = id++,
                BorrowDateTime = DateTime.Parse("2017-01-10"),
                ReturnDateTime = DateTime.Parse("2017-01-17"),
                PersonId = 2,
                BookId = 1
            });
            foreach (var item in lends)
            {
                context.Lends.AddOrUpdate(item);
            }

        }

        private void FillRateList(BookcaseContext context)
        {
            List<Rate> rates = new List<Rate>();
            int id = 1;
            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 10,
                PersonId = 1,
                BookId = 1
            });

            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 9,
                PersonId = 1,
                BookId = 1
            });

            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 9,
                PersonId = 1,
                BookId = 1
            });

            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 8,
                PersonId = 1,
                BookId = 1
            });

            rates.Add(new Rate()
            {
                Id = id++,
                Rating = 9,
                PersonId = 1,
                BookId = 1
            });

            foreach (var item in rates)
            {
                context.Rates.AddOrUpdate(item);
            }
        }


        protected override void Seed(BookcaseContext context)
        {
            FillPersonList(context);
            context.SaveChanges();
            FillBookList(context);
            FillLendList(context);
            FillRateList(context);

            context.SaveChanges();
            base.Seed(context);
        }


        //protected override void Seed(BookcaseData.DAL.BookcaseContext context)
        //{
        //    var persons = new List<Person>
        //    {
        //        new Person {Name = "Paweł", LastName = "Jędrysiak", Id = 1},
        //        new Person {Name = "Żaneta", LastName = "Krawczyk", Id = 2},
        //        new Person {Name = "Piotr", LastName = "Wiśniewski", Id = 3},
        //        new Person {Name = "Paweł", LastName = "Jaworski", Id=4},
        //        new Person {Name = "Łukasz", LastName = "Kłos", Id=5},
        //    };

        //    persons.ForEach(b => context.Persons.AddOrUpdate(b));
        //    context.SaveChanges();


        //    var books = new List<Book>
        //    {
        //        new Book {Title = "Sługa Boży", Writer = "Jacek Piekara", Genre = "Fantasy", OwnerId = persons.Single(p=>p.LastName == "Jędrysiak").Id, Id = 1},
        //        new Book {Title = "Elon Musk", Writer = "Ashlee Vance", Genre = "Bibliography", OwnerId = persons.Single(p=>p.LastName == "Krawczyk").Id, Id =2 },
        //        new Book {Title = "Powrót z Gwiazd", Writer = "Stanisław Lem", Genre = "Sci-Fi", OwnerId = persons.Single(p=>p.LastName == "Wiśniewski").Id, Id = 3},
        //        new Book {Title = "Martin Eden", Writer = "Jack London", Genre = "Novel", OwnerId = persons.Single(p=>p.LastName == "Jaworski").Id, Id = 4},
        //        new Book {Title = "Ostatnie Życzenie", Writer = "Andrzej Sapkowski", Genre = "Fantasy", OwnerId = persons.Single(p=>p.LastName == "Kłos").Id, Id = 5},

        //    };
        //    books.ForEach(s => context.Books.AddOrUpdate(s));
        //    context.SaveChanges();

        //    var lends = new List<Lend>
        //    {
        //        new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Sługa Boży").Id, Id = 1},
        //        new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Elon Musk").Id, Id = 2},
        //        new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Powrót z Gwiazd").Id, Id = 3},
        //        new Lend {BorrowDateTime = DateTime.Parse("2017-01-01"), ReturnDateTime = DateTime.Parse("2017-01-07"), BookId = books.Single(b=>b.Title == "Martin Eden").Id, Id = 4},
        //        new Lend {BorrowDateTime = DateTime.Parse("2017-01-10"), ReturnDateTime = DateTime.Parse("2017-01-17"), BookId = books.Single(b=>b.Title == "Ostatnie Życzenie").Id, Id = 5},

        //    };
        //    lends.ForEach(s => context.Lends.AddOrUpdate(s));
        //    context.SaveChanges();

        //    var rates = new List<Rate>
        //    {
        //        new Rate {Rating = 10, BookId = books.Single(b=>b.Title == "Sługa Boży").Id, Id = 1},
        //        new Rate {Rating = 9, BookId = books.Single(b=>b.Title == "Elon Musk").Id, Id = 2},
        //        new Rate {Rating = 9, BookId = books.Single(b=>b.Title == "Powrót z Gwiazd").Id, Id = 3},
        //        new Rate {Rating = 8, BookId = books.Single(b=>b.Title == "Martin Eden").Id, Id = 4},
        //        new Rate {Rating = 9, BookId = books.Single(b=>b.Title == "Ostatnie Życzenie").Id, Id = 5},
        //    };
        //    rates.ForEach(s => context.Rates.AddOrUpdate(s));
        //    context.SaveChanges();




        //  This method will be called after migrating to the latest version.

        //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
        //  to avoid creating duplicate seed data. E.g.
        //
        //    context.People.AddOrUpdate(
        //      p => p.FullName,
        //      new Owner { FullName = "Andrew Peters" },
        //      new Owner { FullName = "Brice Lambson" },
        //      new Owner { FullName = "Rowan Miller" }
        //    );
        //
    }
}

