﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using BookcaseData.DAL;

namespace BookcaseData.EntityModels
{
    public class Book : Entity
    {
        [Required]
        [Display(Name = "Tytuł")]
        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Autor")]
        [StringLength(50, MinimumLength = 5)]
        public string Writer { get; set; }
        
        [Required]
        [Display(Name = "Gatunek")]
        public string Genre { get; set; }

        [Display(Name = "Właściciel")]
        public int OwnerId { get; set; }
        
        public virtual Person Owner { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
        public virtual ICollection<Lend> Lends { get; set; }
    }
}