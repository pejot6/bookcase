﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using BookcaseData.DAL;

namespace BookcaseData.EntityModels
{
    public class Person : Entity
    {
        [Required]
        [StringLength(15, ErrorMessage = "First name cannot be longer than 15 characters.")]
        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Last name cannot be longer than 50 characters.")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        public string Fullname => Name + " " + LastName;
        

        public virtual ICollection<Book> Books { get; set; }
        public virtual ICollection<Lend> Lends { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
    }
}

