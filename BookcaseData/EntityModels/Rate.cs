﻿using System.ComponentModel.DataAnnotations;
using BookcaseData.DAL;

namespace BookcaseData.EntityModels
{
 
    public class Rate : Entity
    {
        [Display(Name = "Ocena")]
        [Range(0,10)]       
        [DisplayFormat(NullDisplayText = "Brak oceny")]
        public int Rating { get; set; }
        [Display(Name = "Książka")]
        public int BookId { get; set; }
        [Display(Name = "Wypożyczający")]
        public int PersonId { get; set; }

        public virtual Book Book { get; set; }
        public virtual Person Person { get; set; }
    }
}