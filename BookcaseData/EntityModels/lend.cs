﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Web;
using BookcaseData.DAL;

namespace BookcaseData.EntityModels
{
    public class Lend : Entity
    {
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data wypożyczenia")]
        public DateTime BorrowDateTime { get; set; }

        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data oddania")]
        public DateTime ReturnDateTime { get; set; }

        public bool IsLoaned { get; set; }

        [Display(Name = "Książka")]
        public int BookId { get; set; }
        [Display(Name = "Wypożyczający")]
        public int PersonId { get; set; }

        public virtual Book Book { get; set; }
        public virtual Person Person { get; set; }
    }
}