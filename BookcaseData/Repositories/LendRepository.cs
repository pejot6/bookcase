﻿using System.Data.Entity;
using System.Linq;
using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace BookcaseData.Repositories
{
    public class LendRepository : RepositoryBase<Lend>, ILendRepository
    {
        public LendRepository(DbContext context) : base(context)
        {
        }

        //public override Lend GetById(int id)
        //{
        //    return ((BookcaseContext)_context).Lends
        //                                      .Include(x => x.Book)
        //                                      .FirstOrDefault(x => x.Id == id);
        //}
    }
}