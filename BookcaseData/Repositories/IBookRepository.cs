﻿using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace BookcaseData.Repositories
{
    public interface IBookRepository : IRepository<Book>
    {
    }
}
