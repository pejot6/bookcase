﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace BookcaseData.Repositories
{
    public class RateRepository : RepositoryBase<Rate>, IRateRepository
    {
        public RateRepository(DbContext context) : base(context)
        {
            
        }
    }
}