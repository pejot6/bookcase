﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace BookcaseData.Repositories
{
    public class BookRepository : RepositoryBase<Book>, IBookRepository
    {
        public BookRepository(DbContext context) : base(context)
        {
            
        }
    }
}