﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookcaseData.DAL;
using BookcaseData.EntityModels;

namespace BookcaseData.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
    }
}
