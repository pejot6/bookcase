﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookcaseData.EntityModels;

namespace BookcaseData.ViewModels
{
    public class BookVM
    {
        public string Title { get; set; }
        public string Writer { get; set; }
        public string Genre { get; set; }
        public int BookId { get; set; }

       
    }
}
